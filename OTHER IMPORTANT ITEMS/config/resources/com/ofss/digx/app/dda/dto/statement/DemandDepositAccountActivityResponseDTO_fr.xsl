<?xml version="1.0" encoding="UTF-8"?>
	<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:currency="http://www.oracle.com/XSL/Transform/java/com.ofss.digx.appx.helper.CurrencyHelper" xmlns:jdate="http://www.oracle.com/XSL/Transform/java/com.ofss.digx.appx.helper.DateHelper" xmlns:jrbh="http://www.oracle.com/XSL/Transform/java/com.ofss.digx.appx.helper.ResourceBundleHelper" xmlns:jrbb="http://www.oracle.com/XSL/Transform/java/com.ofss.digx.cz.eco.appx.helper.BranchHelper" xmlns:xdofo="http://xmlns.oracle.com/oxp/fo/extensions" xmlns:xdoxliff="urn:oasis:names:tc:xliff:document:1.1" xmlns:xdoxslt="http://www.oracle.com/XSL/Transform/java/oracle.apps.xdo.template.rtf.XSLTFunctions" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" exclude-result-prefixes="jdate jrbh jrbb">
		<xsl:output doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN" indent="yes" />
		<xsl:variable name="nls" select="'resources.nls.DemandDeposit'" />
		<xsl:variable name="nls-common" select="'resources.nls.Common'" />
		<xsl:variable name="branchCode" select="//branchId" />
		<xsl:variable name="branchData">
			<xsl:value-of select="jrbb:fetchBranchName($branchCode)" /> </xsl:variable>
		<xsl:template match="/">
			<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Helvetica" color="#2d2d2d">
				<fo:layout-master-set>
					<fo:simple-page-master master-name="page" page-height="29.7cm" page-width="21cm" margin-top="0.5cm" margin-left="1cm" margin-right="1cm">
						<fo:region-body margin-bottom="1.25cm" margin-top="1cm" background-repeat="no-repeat" background-position-horizontal="center" background-position-vertical="center" />
						<fo:region-before extent="1cm" />
						<fo:region-after extent="1cm" /> </fo:simple-page-master>
					<fo:page-sequence-master master-name="all">
						<fo:repeatable-page-master-alternatives>
							<fo:conditional-page-master-reference master-reference="page" page-position="first" /> </fo:repeatable-page-master-alternatives>
					</fo:page-sequence-master>
				</fo:layout-master-set>
				<fo:page-sequence master-reference="page" initial-page-number="1">
					<fo:static-content flow-name="xsl-region-before">
						<!-- <xsl:call-template name="header" /> -->
						<fo:block-container top="0.01mm" position="absolute">
							<fo:block-container xmlns:fo="http://www.w3.org/1999/XSL/Format">
								<xsl:variable name="logo">
									<xsl:value-of select="jrbh:getString($nls-common, 'bankLogoURL')" /> </xsl:variable>
								<fo:block text-align="right" margin-top="4px">
									<fo:external-graphic src="url({$logo})" content-height="scale-to-fit" height="1.00in" content-width="1.00in" scaling="uniform" /> </fo:block>
							</fo:block-container>
						</fo:block-container>
					</fo:static-content>
					<xsl:call-template name="footer" />
					<fo:flow flow-name="xsl-region-body">
						<xsl:variable name="partyId" select="//party/partyId/value" />
						<fo:block-container height="0.05cm" width="3cm" top="0.01mm" position="absolute">
							<fo:table>
								<fo:table-column column-width="98mm" />
								<fo:table-column column-width="98mm" />
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell>
											<fo:block text-align="left" padding-bottom="2px" font-size="9pt" left="1cm">
												<xsl:value-of select="jrbh:getString($nls, 'activity.name')" /> :
												<fo:inline font-weight="bold">
													<xsl:value-of select="//accountId/displayValue" /> |
													<xsl:value-of select="//party/partyName" /> </fo:inline>
											</fo:block>
											<!-- <fo:block text-align="left" padding-bottom="2px" font-size="9pt" left="1cm">
												<xsl:value-of select="jrbh:getString($nls, 'activity.address')" /> :
												<fo:inline font-weight="bold">
													<xsl:value-of select="jrbp:fetchPartyAddress($partyId)" /> </fo:inline>
											</fo:block> -->
										</fo:table-cell>
										<fo:table-cell>
											<fo:block font-size="8pt" font-family="Helvetica" margin-left="10px" text-align="left">
												<xsl:value-of select="jrbh:getString($nls, 'activity.date')" /> :
												<fo:inline font-weight="bold">
													<xsl:value-of select="jdate:fetchCurrentTime('dd MMM yyyy, HH:mm')" /> </fo:inline>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
							<fo:block white-space-collapse="false" line-height="1pt" top="1mm" left="0cm" position="absolute">
								<fo:table xmlns:fo="http://www.w3.org/1999/XSL/Format" table-layout="auto" background-color="#FFFFFF" line-height="20pt" space-before.optimum="10pt" space-after.optimum="10pt">
									<fo:table-body>
										<fo:table-row background-color="#337ab3">
											<fo:table-cell>
												<fo:block text-align="left" color="white" font-size="8pt" height="5mm" padding-left="3pt" font-weight="bold" padding-top="3pt">
													<xsl:value-of select="jrbh:getString($nls, 'activity.summary')" /> </fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block text-align="left"></fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block text-align="left"></fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block text-align="left"></fo:block>
											</fo:table-cell>
										</fo:table-row>
										<xsl:call-template name="border" />
										<fo:table-row border-width="0.01mm" border-color="#FFFFFF" keep-together="always" background-color="#FFFFFF">
											<fo:table-cell>
												<fo:block space-after="2pt" space-before="2pt" text-align="left" font-size="8pt">
													<xsl:value-of select="jrbh:getString($nls, 'activity.acctNo')" /> </fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block space-after="2pt" space-before="2pt" text-align="left" font-size="8pt" font-weight="bold">
													<xsl:value-of select="//accountId/value" /> </fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block text-align="left" font-size="8pt">
													<xsl:value-of select="jrbh:getString($nls, 'activity.branch')" /> </fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block space-after="2pt" space-before="2pt" text-align="left" font-size="8pt" font-weight="bold">
													<xsl:value-of select="substring-before($branchData,'~')" /> </fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row border-width="0.01mm" border-color="#FFFFFF" keep-together="always" background-color="#FFFFFF">
											<fo:table-cell>
												<fo:block space-after="2pt" space-before="2pt" text-align="left" font-size="8pt">
													<xsl:value-of select="jrbh:getString($nls, 'activity.acctType')" /> </fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block space-after="2pt" space-before="2pt" text-align="left" font-size="8pt" font-weight="bold">
													<xsl:value-of select="jrbh:getString($nls, 'activity.title')" /> </fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block text-align="left" font-size="8pt">
													<xsl:value-of select="jrbh:getString($nls, 'activity.currency')" /> </fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block space-after="2pt" space-before="2pt" text-align="left" font-size="8pt" font-weight="bold">
													<xsl:value-of select="//summary/closingBalance/currency" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row border-width="0.01mm" border-color="#FFFFFF" keep-together="always" background-color="#FFFFFF">
											<fo:table-cell>
												<fo:block space-after="2pt" space-before="2pt" text-align="left" font-size="8pt">
													<xsl:value-of select="jrbh:getString($nls, 'activity.totalDebit')" /> </fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block space-after="2pt" space-before="2pt" text-align="left" font-size="8pt" font-weight="bold">
													<xsl:value-of select="currency:getFormattedCurrencyforApacheXSL(//summary/debitAmount/currency/text(),//summary/debitAmount/amount/text())" />
												</fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block text-align="left" font-size="8pt">
													<xsl:value-of select="jrbh:getString($nls, 'activity.totalCredit')" /> </fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block space-after="2pt" space-before="2pt" text-align="left" font-size="8pt" font-weight="bold">
													<xsl:value-of select="currency:getFormattedCurrencyforApacheXSL(//summary/creditAmount/currency/text(),//summary/creditAmount/amount/text())" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row border-width="0.01mm" border-color="#FFFFFF" keep-together="always" background-color="#FFFFFF">
											<fo:table-cell>
												<fo:block space-after="2pt" space-before="2pt" text-align="left" font-size="8pt">
													<xsl:value-of select="jrbh:getString($nls, 'activity.openingBalance')" /> </fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block space-after="2pt" space-before="2pt" text-align="left" font-size="8pt" font-weight="bold">
													<xsl:value-of select="currency:getFormattedCurrencyforApacheXSL(//summary/openingBalance/currency/text(),//summary/openingBalance/amount/text())" />
												</fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block text-align="left" font-size="8pt">
													<xsl:value-of select="jrbh:getString($nls, 'activity.closingBalance')" /> </fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<xsl:variable name="openBalance" select="//summary/openingBalance/amount" />
												<xsl:variable name="creditAmount" select="//summary/creditAmount/amount" />
												<xsl:variable name="debitAmount" select="//summary/debitAmount/amount" />
												<xsl:variable name="var3" select="$openBalance + $creditAmount - $debitAmount" />
												<fo:block space-after="2pt" space-before="2pt" text-align="left" font-size="8pt" font-weight="bold">
													<xsl:value-of select="currency:getFormattedCurrency(//summary/closingBalance/currency,$var3)" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:block>
						</fo:block-container>
						<fo:block white-space-collapse="false" line-height="1pt" top="1mm" left="0cm" position="absolute">
							<xsl:call-template name="totalDetails" /> </fo:block>
					</fo:flow>
				</fo:page-sequence>
			</fo:root>
		</xsl:template>
		<xsl:template name="totalDetails">
			<xsl:variable name="newline" select="'&#xA;'" />
			<fo:table xmlns:fo="http://www.w3.org/1999/XSL/Format" table-layout="auto" background-color="#FFFFFF" line-height="20pt" space-before.optimum="20pt" space-after.optimum="10pt">
				<fo:table-column column-width="25mm" background-color="#FFFFFF" />
				<fo:table-column column-width="40mm" background-color="#FFFFFF" />
				<fo:table-column column-width="35mm" background-color="#FFFFFF" />
				<fo:table-column column-width="20mm" background-color="#FFFFFF" />
				<fo:table-column column-width="25mm" background-color="#FFFFFF" />
				<fo:table-column column-width="25mm" background-color="#FFFFFF" />
				<fo:table-column column-width="25mm" background-color="#FFFFFF" />
				<fo:table-header>
					<fo:table-row background-color="#337ab3">
						<fo:table-cell number-columns-spanned="7">
							<fo:block text-align="left" color="white" font-size="12pt" height="5mm" padding-left="3pt" font-weight="bold" padding-top="3pt">
								<xsl:value-of select="jrbh:getString($nls, 'activity.accountStatement')" /> </fo:block>
						</fo:table-cell>
					</fo:table-row>
					<xsl:call-template name="border" />
					<fo:table-row>
						<fo:table-cell background-color="#FFFFFF">
							<fo:block text-align="left" font-size="8pt" padding-top="5px" padding-bottom="5px">
								<xsl:value-of select="jrbh:getString($nls, 'activity.txnDate')" /> </fo:block>
						</fo:table-cell>
						<fo:table-cell background-color="#FFFFFF">
							<fo:block text-align="left" font-size="8pt" padding-top="5px" padding-bottom="5px">
								<xsl:value-of select="jrbh:getString($nls, 'activity.description')" /> </fo:block>
						</fo:table-cell>
						<fo:table-cell background-color="#FFFFFF">
							<fo:block text-align="left" font-size="8pt" padding-top="5px" padding-bottom="5px">
								<xsl:value-of select="jrbh:getString($nls, 'activity.referenceNumber')" /> </fo:block>
						</fo:table-cell>
						<fo:table-cell background-color="#FFFFFF">
							<fo:block text-align="left" font-size="8pt" padding-top="5px" padding-bottom="5px">
								<xsl:value-of select="jrbh:getString($nls, 'activity.valueDate')" /> </fo:block>
						</fo:table-cell>
						<fo:table-cell background-color="#FFFFFF">
							<fo:block text-align="right" font-size="8pt" padding-top="5px" padding-bottom="5px" margin-right="10pt">
								<xsl:value-of select="jrbh:getString($nls, 'activity.debitAmount')" /> </fo:block>
						</fo:table-cell>
						<fo:table-cell background-color="#FFFFFF">
							<fo:block text-align="right" font-size="8pt" padding-top="5px" padding-bottom="5px" margin-right="10pt">
								<xsl:value-of select="jrbh:getString($nls, 'activity.creditAmount')" /> </fo:block>
						</fo:table-cell>
						<fo:table-cell background-color="#FFFFFF">
							<fo:block text-align="right" font-size="8pt" padding-top="5px" padding-bottom="5px" margin-right="10pt">
								<xsl:value-of select="jrbh:getString($nls, 'activity.balance')" /> </fo:block>
						</fo:table-cell>
					</fo:table-row>
					<xsl:call-template name="border" /> </fo:table-header>
				<fo:table-body>
					<xsl:for-each select="//items">
						<xsl:sort select="jdate:timeInMillis(transactionDate)" data-type="number" order="descending" />
						<xsl:sort select="key/subSequenceNumber" data-type="number" order="descending" />
						<xsl:variable name="type" select="transactionType" />
						<fo:table-row border-width="0.01mm" border-color="#FFFFFF" keep-together="always" background-color="#FFFFFF">
							<fo:table-cell background-color="#FFFFFF">
								<fo:block text-align="left" background-color="#FFFFFF" font-size="8pt" padding-top="5pt">
									<xsl:value-of select="jdate:format(transactionDate, 'dd')" />
									<xsl:text> </xsl:text>
									<xsl:value-of select="jdate:format(transactionDate,'MMM YYYY')" /> </fo:block>
							</fo:table-cell>
							<fo:table-cell background-color="#FFFFFF">
								<fo:block text-align="left" font-size="8pt" padding-top="5px" padding-bottom="5px" background-color="#FFFFFF" margin-right="20pt">
									<!--xsl:value-of select="substring-before(description,'~')"/-->
									<xsl:value-of select="description" /> </fo:block>
							</fo:table-cell>
							<fo:table-cell background-color="#FFFFFF">
								<fo:block text-align="left" font-size="8pt" padding-top="5px" padding-bottom="5px" background-color="#FFFFFF" margin-right="20pt">
									<!--xsl:value-of select="substring-after(description,'~')"/-->
									<xsl:value-of select="key/transactionReferenceNumber" /> </fo:block>
							</fo:table-cell>
							<fo:table-cell background-color="#FFFFFF">
								<fo:block text-align="left" background-color="#FFFFFF" font-size="8pt" padding-top="5pt">
									<xsl:value-of select="jdate:format(valueDate, 'dd')" />
									<xsl:text> </xsl:text>
									<xsl:value-of select="jdate:format(valueDate,'MMM YYYY')" /> </fo:block>
							</fo:table-cell>
							<fo:table-cell background-color="#FFFFFF">
								<fo:block text-align="right" font-size="8pt" padding-top="5px" padding-bottom="5px" background-color="#FFFFFF" margin-right="10pt">
									<xsl:choose>
										<xsl:when test="$type='DEBIT'">
											<xsl:value-of select="currency:getFormattedCurrencyforApacheXSL(amountInAccountCurrency/currency/text(),amountInAccountCurrency/amount/text())" /> </xsl:when>
										<xsl:otherwise> </xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell background-color="#FFFFFF">
								<fo:block text-align="right" font-size="8pt" padding-top="5px" padding-bottom="5px" background-color="#FFFFFF" margin-right="10pt">
									<xsl:choose>
										<xsl:when test="$type='CREDIT'">
											<xsl:value-of select="currency:getFormattedCurrencyforApacheXSL(amountInAccountCurrency/currency/text(),amountInAccountCurrency/amount/text())" /> </xsl:when>
										<xsl:otherwise> </xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</fo:table-cell>
							<xsl:if test="runningBalance">
								<fo:table-cell>
									<fo:block text-align="right" background-color="#FFFFFF" font-size="8pt" padding-top="5pt" margin-right="10pt">
										<xsl:value-of select="currency:getFormattedCurrencyforApacheXSL(runningBalance/currency/text(),runningBalance/amount/text())" /> </fo:block>
								</fo:table-cell>
							</xsl:if>
						</fo:table-row>
					</xsl:for-each>
				</fo:table-body>
			</fo:table>
		</xsl:template>
		<xsl:template name="header">
			<fo:block-container xmlns:fo="http://www.w3.org/1999/XSL/Format">
				<xsl:variable name="logo">
					<xsl:value-of select="jrbh:getString($nls-common, 'bankLogoURL')" /> </xsl:variable>
				<fo:block text-align="right">
					<fo:external-graphic src="url({$logo})" content-height="scale-to-fit" height="1.00in" content-width="1.00in" scaling="uniform" /> </fo:block>
			</fo:block-container>
		</xsl:template>
		<xsl:template name="footer">
			<fo:static-content xmlns:fo="http://www.w3.org/1999/XSL/Format" flow-name="xsl-region-after">
				<fo:block-container height="0.3cm" width="21cm" top="0.01mm" position="absolute">
					<fo:table>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell>
									<fo:block text-align="left" font-size="8pt" space-before="10pt" space-after.precedence="3">
										<xsl:value-of select="jrbh:getString($nls-common, 'note')" /> </fo:block>
								</fo:table-cell>
							</fo:table-row>
							<xsl:call-template name="border" />
							<fo:table-row>
								<fo:table-cell>
									<fo:block space-before="2pt" text-align="left" font-size="8pt">
										<xsl:value-of select="jrbh:getString($nls-common, 'bankName')" /> :
										<fo:inline>
											<xsl:value-of select="substring-before($branchData,'~')" /> </fo:inline>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
									<fo:block space-before="2pt" text-align="left" font-size="8pt">
										<xsl:value-of select="jrbh:getString($nls, 'activity.address')" /> :
										<fo:inline>
											<xsl:value-of select="substring-after($branchData,'~')" /> </fo:inline>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
									<fo:block space-before="2pt" text-align="left" font-size="8pt">
										<xsl:value-of select="jrbh:getString($nls-common, 'bankEmailtitle')" /> :
										<fo:inline>
											<xsl:value-of select="jrbh:getString($nls-common, 'bankEmail')" /> </fo:inline>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
									<fo:block space-before="2pt" space-after="2pt" text-align="left" font-size="8pt">
										<xsl:value-of select="jrbh:getString($nls-common, 'bankWebsitetitle')" /> :
										<fo:inline>
											<xsl:value-of select="jrbh:getString($nls-common, 'bankWebsite')" /> </fo:inline>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<xsl:call-template name="border" /> </fo:table-body>
					</fo:table>
					<fo:table>
						<fo:table-column column-width="14px" />
						<fo:table-column />
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell>
									<fo:block space-before="2pt" font-size="9pt" background-color="#bbbbbb" font-family="Helvetica" text-align="center" height="0.7cm">
										<fo:page-number /> </fo:block>
								</fo:table-cell>
								<fo:table-cell>
									<fo:block space-before="2pt" font-size="8pt" font-family="Helvetica" margin-left="10px">
										<xsl:value-of select="jdate:fetchCurrentTime('dd MMM yyyy, HH:mm')" /> </fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:block-container>
			</fo:static-content>
		</xsl:template>
		<xsl:template xmlns:fo="http://www.w3.org/1999/XSL/Format" name="border">
			<fo:table-row>
				<fo:table-cell number-columns-spanned="7">
					<fo:block background-color="#337ab3" height="1px" /> </fo:table-cell>
			</fo:table-row>
		</xsl:template>
		<xsl:template xmlns:fo="http://www.w3.org/1999/XSL/Format" name="draftBorder">
			<fo:table-row>
				<fo:table-cell number-columns-spanned="6">
					<fo:block background-color="#337ab3" height="1px" /> </fo:table-cell>
			</fo:table-row>
		</xsl:template>
	</xsl:stylesheet>