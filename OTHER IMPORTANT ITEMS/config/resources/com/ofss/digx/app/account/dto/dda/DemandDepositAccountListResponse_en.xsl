<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:currency="http://www.oracle.com/XSL/Transform/java/com.ofss.digx.appx.helper.CurrencyHelper" xmlns:jdate="http://www.oracle.com/XSL/Transform/java/com.ofss.digx.appx.helper.DateHelper" xmlns:jrbh="http://www.oracle.com/XSL/Transform/java/com.ofss.digx.appx.helper.ResourceBundleHelper" xmlns:xdofo="http://xmlns.oracle.com/oxp/fo/extensions" xmlns:xdoxliff="urn:oasis:names:tc:xliff:document:1.1" xmlns:xdoxslt="http://www.oracle.com/XSL/Transform/java/oracle.apps.xdo.template.rtf.XSLTFunctions" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" exclude-result-prefixes="jdate java jrbh">
   <xsl:output doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN" indent="yes" />
   <xsl:variable name="nls" select="'resources.nls.DemandDeposit'" />
   <xsl:variable name="nls-common" select="'resources.nls.Common'" />
   <xsl:template match="/">
      <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Helvetica" color="#2d2d2d">
         <fo:layout-master-set>
            <fo:simple-page-master master-name="page" page-height="29.7cm" page-width="21cm" margin-top="0.5cm" margin-left="1cm" margin-right="1cm">
               <fo:region-body margin-bottom="1.25cm" margin-top="1cm" background-repeat="no-repeat" background-position-horizontal="center" background-position-vertical="center" />
		<fo:region-before extent="1cm" />
               
               <fo:region-after extent="1cm" />
            </fo:simple-page-master>
            <fo:page-sequence-master master-name="all">
               <fo:repeatable-page-master-alternatives>
                  <fo:conditional-page-master-reference master-reference="page" page-position="first" />
               </fo:repeatable-page-master-alternatives>
            </fo:page-sequence-master>
         </fo:layout-master-set>
         <fo:page-sequence master-reference="page" initial-page-number="1">
            <fo:static-content flow-name="xsl-region-before">
               <fo:block-container height="0.1cm" width="3cm" top="0.01mm" position="absolute">
                  
                  <fo:table>
                     <fo:table-column />
                     <fo:table-column />
                     <fo:table-body>
                        <fo:table-row>
                           <fo:table-cell>
                              <fo:block text-align="left" padding-bottom="2px" font-size="14pt" left="1cm">
                                 <xsl:value-of select="jrbh:getString($nls, 'summary.title')" />
                              </fo:block>
                           </fo:table-cell>
                           <fo:table-cell>
                              <xsl:call-template name="header" />
                           </fo:table-cell>
                        </fo:table-row>
                     </fo:table-body>
                  </fo:table>
                  
               </fo:block-container>
            </fo:static-content>
            <xsl:call-template name="footer" />
            <fo:flow flow-name="xsl-region-body">
               <fo:block white-space-collapse="false" line-height="1pt" top="1mm" left="0cm" position="absolute">
                  <xsl:call-template name="totalDetails" />
               </fo:block>
            </fo:flow>
            <!--<xsl:call-template name="footer" />-->
         </fo:page-sequence>
      </fo:root>
   </xsl:template>
   <xsl:template name="totalDetails">
      <xsl:variable name="newline" select="'&#xA;'" />
      <xsl:for-each select="/demandDepositAccountListResponse/summary/items">
		<xsl:variable name="partyId" select="party/value" />
		<fo:table xmlns:fo="http://www.w3.org/1999/XSL/Format" table-layout="auto" background-color="#FFFFFF" >
			<fo:table-column column-width="65mm" background-color="#FFFFFF" />
			<fo:table-body>
				<fo:table-row background-color="#FFFFFF">
					<fo:table-cell background-color="#FFFFFF">
						<fo:block text-align="left" font-size="10pt" padding-top="5px" padding-bottom="5px">
							<xsl:value-of select="jrbh:getString($nls, 'summary.partyId')" /><xsl:text>: </xsl:text><fo:inline font-weight="bold"><xsl:value-of select="party/displayValue" /></fo:inline>
						</fo:block>
						<fo:block text-align="left" font-size="10pt" padding-top="5px" padding-bottom="5px">
							<xsl:value-of select="jrbh:getString($nls, 'summary.partyName')" /><xsl:text>: </xsl:text><fo:inline font-weight="bold"><xsl:value-of select="partyName" /></fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		<fo:table xmlns:fo="http://www.w3.org/1999/XSL/Format" table-layout="auto" background-color="#FFFFFF" line-height="20pt" space-before.optimum="20pt" space-after.optimum="10pt">
			<fo:table-column column-width="65mm" background-color="#FFFFFF" />
			<fo:table-column column-width="35mm" background-color="#FFFFFF" />
			<fo:table-column column-width="40mm" background-color="#FFFFFF" />
			<fo:table-column column-width="55mm" background-color="#FFFFFF" />
			<fo:table-header>
				<fo:table-row background-color="#337ab3" border-width="0.5mm" border-color="#337ab3">
					<fo:table-cell>
						<fo:block text-align="left" font-size="10pt" padding-left="5px" padding-top="5px" padding-bottom="5px" color="#FFFFFF">
							<xsl:value-of select="jrbh:getString($nls, 'summary.accountNameAndNumber')" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block text-align="left" font-size="10pt" padding-top="5px" padding-bottom="5px" color="#FFFFFF">
							<xsl:value-of select="jrbh:getString($nls, 'summary.accountType')" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block text-align="left" font-size="10pt" padding-top="5px" padding-bottom="5px" color="#FFFFFF">
							<xsl:value-of select="jrbh:getString($nls, 'summary.holdingPattern')" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block text-align="right" font-size="10pt" padding-top="5px" padding-bottom="5px" margin-right="10pt" color="#FFFFFF">
							<xsl:value-of select="jrbh:getString($nls, 'summary.netBalance')" />
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<xsl:for-each select="/demandDepositAccountListResponse/accounts[partyId/value=$partyId]">
					<fo:table-row xmlns:fo="http://www.w3.org/1999/XSL/Format" border-width="0.5mm" keep-together="always" border-bottom-color="#D3D3D3" border-top-color="#FFFFFF" border-left-color="#FFFFFF" border-right-color="#FFFFFF">
						<fo:table-cell background-color="#FFFFFF">
							<fo:block text-align="left" background-color="#FFFFFF" font-size="10pt" padding-top="5pt">
								<xsl:value-of select="displayName" />
							</fo:block>
							<fo:block text-align="left" background-color="#FFFFFF" font-size="10pt" padding-top="5pt">
								<xsl:value-of select="id/displayValue" />
							</fo:block>
						</fo:table-cell>
						<fo:table-cell background-color="#FFFFFF">
							<fo:block text-align="left" font-size="10pt" padding-top="5px" padding-bottom="5px" background-color="#FFFFFF" margin-right="20pt">
								<xsl:choose>
									<xsl:when test="ddaAccountType='SAVING'">
										<xsl:value-of select="jrbh:getString($nls, 'summary.accountTypeSavings')" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="jrbh:getString($nls, 'summary.accountTypeCurrent')" />
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell background-color="#FFFFFF">
							<fo:block text-align="left" font-size="10pt" padding-top="5px" padding-bottom="5px" background-color="#FFFFFF" margin-right="20pt">
								<xsl:choose>
									<xsl:when test="holdingPattern='SINGLE'">
										<xsl:value-of select="jrbh:getString($nls, 'summary.holdingPatternSingle')" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="jrbh:getString($nls, 'summary.holdingPatternJoint')" />
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block text-align="right" background-color="#FFFFFF" font-size="10pt" padding-top="5pt" margin-right="10pt">
								<xsl:value-of select="currency:getFormattedCurrencyforApacheXSL(availableBalance/currency/text(), availableBalance/amount/text())" />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:for-each>
			</fo:table-body>
		</fo:table>
      </xsl:for-each>
   </xsl:template>
   
   <xsl:template name="header">
      <fo:block-container xmlns:fo="http://www.w3.org/1999/XSL/Format">
         <!--<fo:block text-align="right">
            <fo:external-graphic src="url({jrbh:getString($nls-common, 'bankLogoURL')})" content-alignment="right" content-height="scale-to-fit" height="1.00in" content-width="1.00in" scaling="non-uniform" />
         </fo:block>-->
<xsl:variable name="logo">
					<xsl:value-of select="jrbh:getString($nls-common, 'bankLogoURL')" /> </xsl:variable>
				<fo:block text-align="right">
					<fo:external-graphic src="url({$logo})" content-height="scale-to-fit" height="1.00in" content-width="1.00in" scaling="uniform" /> </fo:block>
         <fo:block text-align="right" font-size="9pt" font-family="Helvetica" left="1cm">
            <xsl:value-of select="jrbh:getString($nls-common, 'bankName')" />
         </fo:block>
      </fo:block-container>
   </xsl:template>
   <xsl:template name="footer">
      <fo:static-content xmlns:fo="http://www.w3.org/1999/XSL/Format" flow-name="xsl-region-after">
         <fo:block-container height="0.3cm" width="21cm" top="0.01mm" position="absolute">
            <fo:block wrap-option="no-wrap" overflow="visible" margin-left="2mm" margin-bottom="10px">
              <!-- <fo:leader leader-length="185mm" leader-pattern="rule" rule-style="solid" rule-thickness="0.4mm" color="#999999" />-->
            </fo:block>
            <fo:table>
               <fo:table-column column-width="14px" />
               <fo:table-column />
               <fo:table-body>
                  <fo:table-row>
                     <fo:table-cell>
                        <fo:block font-size="9pt" background-color="#bbbbbb" font-family="Helvetica" text-align="center" height="0.7cm">
                           <fo:page-number />
                        </fo:block>
                     </fo:table-cell>
                     <fo:table-cell>
                        <fo:block font-size="10pt" font-family="Helvetica" margin-left="10px">
                           <xsl:value-of select="jdate:fetchCurrentTime('dd MMM yyyy, HH:mm')" />
                        </fo:block>
                     </fo:table-cell>
                  </fo:table-row>
               </fo:table-body>
            </fo:table>
         </fo:block-container>
      </fo:static-content>
   </xsl:template>
   <xsl:template xmlns:fo="http://www.w3.org/1999/XSL/Format" name="border">
      <fo:table-row>
         <fo:table-cell number-columns-spanned="7">
            <fo:block background-color="#337ab3" height="1px" />
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template xmlns:fo="http://www.w3.org/1999/XSL/Format" name="draftBorder">
      <fo:table-row>
         <fo:table-cell number-columns-spanned="6">
            <fo:block background-color="#337ab3" height="1px" />
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
</xsl:stylesheet>